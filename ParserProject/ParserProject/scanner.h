#include<iostream>
#include<fstream>
#include<string>
#include <map>
#include <iterator>
using namespace std;

//=====================================================
// File scanner.cpp written by: Group Number: 23 
//=====================================================

// --------- DFAs ---------------------------------
// Q0 = QN Q0' = QV
enum states { QV, QC, QCON, QCON1, QS, QT, QN };


bool mytoken(string s)
{
  int state = 0;
  int char_pos = 0;

  while (s[char_pos] != '\0') 
    {
      if (state == 0 && s[char_pos] == 'a')
      state = 1;
      else
      if (state == 1 && s[char_pos] == 'b')
      state = 2;
      else
      if (state == 2 && s[char_pos] == 'b')
      state = 2;
      else
	  return(false);
      char_pos++;
    }//end of while

  // where did I end up????
  if (state == 2) return(true);  // end in a final state
   else return(false);
}

// ** WORD DFA 
// ** Done by: Billy Cole
// ** RE: (vowel | vowel n | consonant vowel | consonant vowel n |
//         consonant - pair vowel | consonant - pair vowel n) ^ +


bool word_dfa(string word)
{
	states state   = QV;
	int char_pos = 0;


	while (word[char_pos] != '\0')
	{
		// QV (Q0')
		if (state == QV && (word[char_pos] == 'a' || word[char_pos] == 'e' || word[char_pos] == 'i' ||
			word[char_pos] == 'o' || word[char_pos] == 'u' || word[char_pos] == 'I' || word[char_pos] == 'E'))
		{
			state = QV;
		}
		else if (state == QV && (word[char_pos] == 'n'))
		{
			state = QN;
		}
		else if (state == QV && (word[char_pos] == 'b' || word[char_pos] == 'g' || word[char_pos] == 'h' ||
			     word[char_pos] == 'k' || word[char_pos] == 'm' || word[char_pos] == 'p' || word[char_pos] == 'r'))
		{
			state = QCON1;
		}
		else if (state == QV && (word[char_pos] == 'd' || word[char_pos] == 'j' || word[char_pos] == 'w' ||
         		 word[char_pos] == 'y' || word[char_pos] == 'z'))
		{
			state = QCON;
		}
		else if (state == QV && (word[char_pos] == 's'))
		{
			state = QS;
		}
		else if (state == QV && (word[char_pos] == 'c'))
		{
			state = QC;
		}
		else if (state == QV && word[char_pos] == 't')
		{
			state = QT;
		}
		// QN (Q0)
		else if (state == QN && (word[char_pos] == 'a' || word[char_pos] == 'e' || word[char_pos] == 'i' ||
			word[char_pos] == 'o' || word[char_pos] == 'u' || word[char_pos] == 'I' || word[char_pos] == 'E'))
		{
			state = QV;
		}
		else if (state == QN && (word[char_pos] == 'b' || word[char_pos] == 'g' || word[char_pos] == 'h' ||
			word[char_pos] == 'k' || word[char_pos] == 'm' || word[char_pos] == 'p' || word[char_pos] == 'r'))
		{
			state = QCON1;
		}
		else if (state == QN && (word[char_pos] == 'd' || word[char_pos] == 'j' || word[char_pos] == 'w' ||
			word[char_pos] == 'y' || word[char_pos] == 'z'))
		{
			state = QCON;
		}
		else if (state == QN && (word[char_pos] == 's'))
		{
			state = QS;
		}
		else if (state == QN && (word[char_pos] == 'c'))
		{
			state = QC;
		}
		else if (state == QV && (word[char_pos] == 't'))
		{
			state = QT;
		}

		// QCON1
		else if (state == QCON1 && (word[char_pos] == 'a' || word[char_pos] == 'e' || word[char_pos] == 'i' ||
			word[char_pos] == 'o' || word[char_pos] == 'u' || word[char_pos] == 'I' || word[char_pos] == 'E'))
		{
			state = QV;
		}
		else if (state == QCON1 && (word[char_pos] == 'y'))
		{
			state = QCON;
		}

		// QCON
		else if (state == QCON && (word[char_pos] == 'a' || word[char_pos] == 'e' || word[char_pos] == 'i' ||
			word[char_pos] == 'o' || word[char_pos] == 'u' || word[char_pos] == 'I' || word[char_pos] == 'E'))
		{
			state = QV;
		}

		// QC
		else if (state == QC && (word[char_pos] == 'h'))
		{
			state = QCON;
		}

		// QT
		else if (state == QT && (word[char_pos] == 'a' || word[char_pos] == 'e' || word[char_pos] == 'i' ||
			word[char_pos] == 'o' || word[char_pos] == 'u' || word[char_pos] == 'I' || word[char_pos] == 'E'))
		{
			state = QV;
		}
		else if (state == QT && (word[char_pos] == 's'))
		{
			state = QCON;
		}

		// QS
		else if (state == QS && (word[char_pos] == 'a' || word[char_pos] == 'e' || word[char_pos] == 'i' ||
			word[char_pos] == 'o' || word[char_pos] == 'u' || word[char_pos] == 'I' || word[char_pos] == 'E'))
		{
			state = QV;
		}
		else if (state == QS && (word[char_pos] == 'h'))
		{
			state = QCON;
		}
		else
		{
		    return false;
		}

		char_pos++;
	}

	if (state == QN || state == QV)
	{
		return true;
	}

	return false;
}

bool period_dfa(string word)
{
	int state = 0;
	int char_pos = 0;

	while (word[char_pos] != '\0')
	{
		if (state == 0 && word[char_pos] == '.')
		{
			state = 1;
		}
		else
		{
			return false;
		}

		char_pos++;
	}

	if (state == 1)
	{
		return true;
	}

	return false;
}

// ** Add the PERIOD DFA here
// ** Done by: Billy Cole

// -----  Tables -------------------------------------

// ** Update the tokentype to be WORD1, WORD2, PERIOD, ERROR, etc.
// Feel free to add a tokentype for the end-of-file marker.
enum tokentype { ERROR, WORD1, WORD2, PERIOD, VERB, VERBNEG, 
	             VERBPAST, VERBPASTNEG, IS, WAS, OBJECT, SUBJECT,
                 DESTINATION, PRONOUN, CONNECTOR, EOFM
               };


// set up names for the display names of tokens
string token_name[30] = { "ERROR", "WORD1", "WORD2", "PERIOD","VERB", "VERBNEG",
                         "VERBPAST", "VERBPASTNEG", "IS", "WAS", "OBJECT", "SUBJECT",
                          "DESTINATION", "PRONOUN", "CONNECTOR", "EOFM"
                       }; 

// ** Need the reservedwords table to be set up here.
// ** Do not require any file input for this.
// ** a.out should work without any additional files.
std::map<string, tokentype> reserved_words;

void createReservedWordsTable()
{
	reserved_words.insert(pair<string, tokentype>("masu",         VERB));
	reserved_words.insert(pair<string, tokentype>("masen",        VERBNEG));
	reserved_words.insert(pair<string, tokentype>("mashita",      VERBPAST));
	reserved_words.insert(pair<string, tokentype>("masendeshita", VERBPASTNEG));
	reserved_words.insert(pair<string, tokentype>("desu",         IS));
	reserved_words.insert(pair<string, tokentype>("deshita",      WAS));
	reserved_words.insert(pair<string, tokentype>("o",            OBJECT));
	reserved_words.insert(pair<string, tokentype>("wa",           SUBJECT));
	reserved_words.insert(pair<string, tokentype>("ni",           DESTINATION));
	reserved_words.insert(pair<string, tokentype>("watashi",      PRONOUN));
	reserved_words.insert(pair<string, tokentype>("anata",        PRONOUN));
	reserved_words.insert(pair<string, tokentype>("kare",         PRONOUN));
	reserved_words.insert(pair<string, tokentype>("kanojo",       PRONOUN));
	reserved_words.insert(pair<string, tokentype>("sore",         PRONOUN));
	reserved_words.insert(pair<string, tokentype>("mata",         CONNECTOR));
	reserved_words.insert(pair<string, tokentype>("soshite",      CONNECTOR));
	reserved_words.insert(pair<string, tokentype>("shikashi",     CONNECTOR));
	reserved_words.insert(pair<string, tokentype>("dakara",       CONNECTOR));
	reserved_words.insert(pair<string, tokentype>("eofm",         EOFM));

}

// ------------ Scaner and Driver ----------------------- 

ifstream fin;  // global stream for reading from the input file

// Scanner processes only one word each time it is called
// Gives back the token type and the word itself
// ** Done by: 
int scanner(tokentype& a, string& w)
{
	// ** Grab the next word from the file via fin
 	fin  >> w;

	cout << "Calling scanner with word: " << w << endl;
/*
  2. Call the token functions one after another (if-then-else)
     And generate a lexical error message if both DFAs failed.
     Let the token_type be ERROR in that case.
*/
	bool accepted = word_dfa(w);

	if (period_dfa(w))
	{
		a = PERIOD;
		return 1;
	}

	// Did we get a reserved word?
	auto it = reserved_words.find(w);

	if (it != reserved_words.end())
	{
		a = reserved_words[w];
		return 1;
	}

	// Is our word a WORD1 or WORD2?
	if (accepted && (w[w.length() - 1] == 'I' || w[w.length() - 1] == 'E'))
	{
		a = WORD2;
		return 1;
	}

	if (accepted)
	{
		a = WORD1;
		return 1;
	}

	// Was there an error?
	if (!accepted)
	{
		a = ERROR;
		cout << "Lexical Error: " << w << " is not a word in the language." << endl;
		return -1;
	}

	return 0;

}//the end of scanner



// The temporary test driver to just call the scanner repeatedly  
// This will go away after this assignment
// DO NOT CHANGE THIS!!!!!! 
// Done by:  Rika
/*
int main()
{
  tokentype thetype;
  string theword; 
  string filename;

  createReservedWordsTable();

  cout << "Enter the input file name: ";
  cin >> filename;

  fin.open(filename.c_str());

  //the loop continues until eofm is returned.
  while (true)
  {
	  scanner(thetype, theword);  // call the scanner
	  if (theword == "eofm") break;  // stop now

	  //cout << "Type is:" << tokenName[thetype] << endl;
	  //cout << "Word is:" << theword << endl << endl << endl;

	  cout << "The word " << "\"" << theword << "\"" << " is type " << tokenName[thetype] << endl << endl;
  }

  cout << "End of file is encountered." << endl;
  fin.close();

}// end
*/
