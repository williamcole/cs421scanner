#include<iostream>
#include<fstream>
#include<string>
#include <map>
#include <iterator>
using namespace std;

//=====================================================
// File scanner.cpp written by: Group Number: 23 
//=====================================================

// --------- DFAs ---------------------------------
// Q0 = QN Q0' = QV
enum states { QV, QC, QCON, QCON1, QS, QT, QN };


bool mytoken(string s)
{
  int state = 0;
  int charpos = 0;

  while (s[charpos] != '\0') 
    {
      if (state == 0 && s[charpos] == 'a')
      state = 1;
      else
      if (state == 1 && s[charpos] == 'b')
      state = 2;
      else
      if (state == 2 && s[charpos] == 'b')
      state = 2;
      else
	  return(false);
      charpos++;
    }//end of while

  // where did I end up????
  if (state == 2) return(true);  // end in a final state
   else return(false);
}

// ** WORD DFA 
// ** Done by: Billy Cole
// ** RE: (vowel | vowel n | consonant vowel | consonant vowel n |
//         consonant - pair vowel | consonant - pair vowel n) ^ +


bool wordDFA(string word)
{
	states state   = QV;
	int charPos = 0;


	while (word[charPos] != '\0')
	{
		// QV (Q0')
		if (state == QV && (word[charPos] == 'a' || word[charPos] == 'e' || word[charPos] == 'i' ||
			word[charPos] == 'o' || word[charPos] == 'u' || word[charPos] == 'I' || word[charPos] == 'E'))
		{
			state = QV;
		}
		else if (state == QV && (word[charPos] == 'n'))
		{
			state = QN;
		}
		else if (state == QV && (word[charPos] == 'b' || word[charPos] == 'g' || word[charPos] == 'h' ||
			     word[charPos] == 'k' || word[charPos] == 'm' || word[charPos] == 'p' || word[charPos] == 'r'))
		{
			state = QCON1;
		}
		else if (state == QV && (word[charPos] == 'd' || word[charPos] == 'j' || word[charPos] == 'w' ||
         		 word[charPos] == 'y' || word[charPos] == 'z'))
		{
			state = QCON;
		}
		else if (state == QV && (word[charPos] == 's'))
		{
			state = QS;
		}
		else if (state == QV && (word[charPos] == 'c'))
		{
			state = QC;
		}
		else if (state == QV && word[charPos] == 't')
		{
			state = QT;
		}
		// QN (Q0)
		else if (state == QN && (word[charPos] == 'a' || word[charPos] == 'e' || word[charPos] == 'i' ||
			word[charPos] == 'o' || word[charPos] == 'u' || word[charPos] == 'I' || word[charPos] == 'E'))
		{
			state = QV;
		}
		else if (state == QN && (word[charPos] == 'b' || word[charPos] == 'g' || word[charPos] == 'h' ||
			word[charPos] == 'k' || word[charPos] == 'm' || word[charPos] == 'p' || word[charPos] == 'r'))
		{
			state = QCON1;
		}
		else if (state == QN && (word[charPos] == 'd' || word[charPos] == 'j' || word[charPos] == 'w' ||
			word[charPos] == 'y' || word[charPos] == 'z'))
		{
			state = QCON;
		}
		else if (state == QN && (word[charPos] == 's'))
		{
			state = QS;
		}
		else if (state == QN && (word[charPos] == 'c'))
		{
			state = QC;
		}
		else if (state == QV && (word[charPos] == 't'))
		{
			state = QT;
		}

		// QCON1
		else if (state == QCON1 && (word[charPos] == 'a' || word[charPos] == 'e' || word[charPos] == 'i' ||
			word[charPos] == 'o' || word[charPos] == 'u' || word[charPos] == 'I' || word[charPos] == 'E'))
		{
			state = QV;
		}
		else if (state == QCON1 && (word[charPos] == 'y'))
		{
			state = QCON;
		}

		// QCON
		else if (state == QCON && (word[charPos] == 'a' || word[charPos] == 'e' || word[charPos] == 'i' ||
			word[charPos] == 'o' || word[charPos] == 'u' || word[charPos] == 'I' || word[charPos] == 'E'))
		{
			state = QV;
		}

		// QC
		else if (state == QC && (word[charPos] == 'h'))
		{
			state = QCON;
		}

		// QT
		else if (state == QT && (word[charPos] == 'a' || word[charPos] == 'e' || word[charPos] == 'i' ||
			word[charPos] == 'o' || word[charPos] == 'u' || word[charPos] == 'I' || word[charPos] == 'E'))
		{
			state = QV;
		}
		else if (state == QT && (word[charPos] == 's'))
		{
			state = QCON;
		}

		// QS
		else if (state == QS && (word[charPos] == 'a' || word[charPos] == 'e' || word[charPos] == 'i' ||
			word[charPos] == 'o' || word[charPos] == 'u' || word[charPos] == 'I' || word[charPos] == 'E'))
		{
			state = QV;
		}
		else if (state == QS && (word[charPos] == 'h'))
		{
			state = QCON;
		}
		else
		{
		    return false;
		}

		charPos++;
	}

	if (state == QN || state == QV)
	{
		return true;
	}

	return false;
}


// PERIOD DFA
// Done by: Billy 
bool periodDFA(string word)
{
	int state = 0;
	int charPos = 0;

	while (word[charPos] != '\0')
	{
		if (state == 0 && word[charPos] == '.')
		{
			state = 1;
		}
		else
		{
			return false;
		}

		charPos++;
	}

	if (state == 1)
	{
		return true;
	}

	return false;
}
// -----  Tables -------------------------------------

// ** Update the tokentype to be WORD1, WORD2, PERIOD, ERROR, etc.
// Feel free to add a tokentype for the end-of-file marker.
enum tokentype { ERROR, WORD1, WORD2, PERIOD, VERB, VERBNEG, 
	             VERBPAST, VERBPASTNEG, IS, WAS, OBJECT, SUBJECT,
                 DESTINATION, PRONOUN, CONNECTOR, EOFM
               };


// set up names for the display names of tokens
string tokenName[30] = { "ERROR", "WORD1", "WORD2", "PERIOD","VERB", "VERBNEG",
                         "VERBPAST", "VERBPASTNEG", "IS", "WAS", "OBJECT", "SUBJECT",
                          "DESTINATION", "PRONOUN", "CONNECTOR", "EOFM"
                       }; 

// ** Need the reservedwords table to be set up here.
// ** Do not require any file input for this.
// ** a.out should work without any additional files.
std::map<string, tokentype> reservedWords;

// DONE BY: Billy
void createReservedWordsTable()
{
	reservedWords.insert(pair<string, tokentype>("masu",         VERB));
	reservedWords.insert(pair<string, tokentype>("masen",        VERBNEG));
	reservedWords.insert(pair<string, tokentype>("mashita",      VERBPAST));
	reservedWords.insert(pair<string, tokentype>("masendeshita", VERBPASTNEG));
	reservedWords.insert(pair<string, tokentype>("desu",         IS));
	reservedWords.insert(pair<string, tokentype>("deshita",      WAS));
	reservedWords.insert(pair<string, tokentype>("o",            OBJECT));
	reservedWords.insert(pair<string, tokentype>("wa",           SUBJECT));
	reservedWords.insert(pair<string, tokentype>("ni",           DESTINATION));
	reservedWords.insert(pair<string, tokentype>("watashi",      PRONOUN));
	reservedWords.insert(pair<string, tokentype>("anata",        PRONOUN));
	reservedWords.insert(pair<string, tokentype>("kare",         PRONOUN));
	reservedWords.insert(pair<string, tokentype>("kanojo",       PRONOUN));
	reservedWords.insert(pair<string, tokentype>("sore",         PRONOUN));
	reservedWords.insert(pair<string, tokentype>("mata",         CONNECTOR));
	reservedWords.insert(pair<string, tokentype>("soshite",      CONNECTOR));
	reservedWords.insert(pair<string, tokentype>("shikashi",     CONNECTOR));
	reservedWords.insert(pair<string, tokentype>("dakara",       CONNECTOR));
	reservedWords.insert(pair<string, tokentype>("eofm",         EOFM));

}

// ------------ Scaner and Driver ----------------------- 

ifstream fin;  // global stream for reading from the input file

// Scanner processes only one word each time it is called
// Gives back the token type and the word itself
// ** Done by: Jordan & Peter
int scanner(tokentype& a, string& w)
{
	// ** Grab the next word from the file via fin
 	fin  >> w;
/*
  2. Call the token functions one after another (if-then-else)
     And generate a lexical error message if both DFAs failed.
     Let the token_type be ERROR in that case.
*/
	bool accepted = wordDFA(w);

	if (periodDFA(w))
	{
		a = PERIOD;
		return 1;
	}

	// Did we get a reserved word?
	auto it = reservedWords.find(w);

	if (it != reservedWords.end())
	{
		a = reservedWords[w];
		return 1;
	}

	// Is our word a WORD1 or WORD2?
	if (accepted && (w[w.length() - 1] == 'I' || w[w.length() - 1] == 'E'))
	{
		a = WORD2;
		return 1;
	}

	if (accepted)
	{
		a = WORD1;
		return 1;
	}

	// Was there an error?
	if (!accepted)
	{
		a = ERROR;
		cout << "Lexical Error: " << w << " is not a word in the language." << endl;
		return -1;
	}

	return 0;

}//the end of scanner



// The temporary test driver to just call the scanner repeatedly  
// This will go away after this assignment
// DO NOT CHANGE THIS!!!!!! 
// Done by:  Rika
int main()
{
  tokentype thetype;
  string theword; 
  string filename;

  createReservedWordsTable();

  cout << "Enter the input file name: ";
  cin >> filename;

  fin.open(filename.c_str());

  //the loop continues until eofm is returned.
  while (true)
  {
	  scanner(thetype, theword);  // call the scanner
	  if (theword == "eofm") break;  // stop now

	  //cout << "Type is:" << tokenName[thetype] << endl;
	  //cout << "Word is:" << theword << endl << endl << endl;

	  cout << "The word " << "\"" << theword << "\"" << " is type " << tokenName[thetype] << endl << endl;
  }

  cout << "End of file is encountered." << endl;
  fin.close();

}// end

