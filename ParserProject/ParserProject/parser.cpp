#include<iostream>
#include<fstream>
#include<string>
using namespace std;
#include "scanner.h"

// INSTRUCTION:  Complete all ** parts.
// You may use any method to connect this file to scanner.cpp
// that you had written.  
// You can copy scanner.cpp here by cp ../ScannerFiles/scanner.cpp .
// -----------------------------------------------------

//=================================================
// File parser.cpp written by Group Number: 23
//=================================================

// ----- LEFT REFACTORED RULE ----- //
/*
<story> :: = <s> { <s> }
<s> : [CONNECTOR] <noun> SUBJECT <after subject>
<after subject> ::= <verb> <tense> PERIOD | <noun> <after noun>
<after noun> ::= <be> PERIOD | DESTINATION | <verb> <tense> PERIOD | OBJECT <after object>
<after object> ::= <verb> <tense> PERIOD | <noun> DESTINATION <verb <tense> PERIOD
<noun> ::= WORD1 | PRONOUN
<verb> ::= WORD2
<be> ::= IS | WAS
<tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG

*/

// ----- Utility and Globals -----------------------------------
tokentype saved_token = PRONOUN;
string saved_lexeme = "";
bool token_available = false;


/************* FUNCTION DECLARATIONS ***************/
bool match(tokentype expected_token);
tokentype next_token();
void syntax_error1(string expectedType, string errorWord);
void syntax_error2(string errorWord, string functionName);
void story();
void end(tokentype endToken);
void s();
void after_subject();
void after_noun();
void after_object();
void noun();
void be();
void verb();
void tense();



// ** Need syntaxerror1 and syntaxerror2 functions (each takes 2 args)
// ** Be sure to put the name of the programmer above each function
// i.e. Done by: Peter
void syntax_error1(string expectedType, string errorType)
{
	cout << "SYNTAX ERROR: " << "Expected token type: " << expectedType
		<< " but found: " << errorType << endl;

	exit(-1);
}

// Done by: Peter
void syntax_error2(string errorWord, string functionName)
{
	cout << "SYNTAX ERROR: " << "unexpected word: " << errorWord
		<< " found in: " << functionName << endl;

	exit(-1);

}


// ** Need the updated match and next_token (with 2 global vars)
// ** Be sure to put the name of the programmer above each function
// removes the currently saved token
// i.e. Done by: Billy
bool match(tokentype expected_token)
{
	if (next_token() != expected_token)
	{ 
		syntax_error1(token_name[expected_token], saved_lexeme);
	}
	else
	{
		cout << "Matched " << token_name[expected_token] << endl;
		token_available = false;
		return true;
	}

	return false;
}

// Checks if there is a saved token available
// If not call scanner and get a new one then return it
// Done by: Billy 
tokentype next_token()
{
	// No token saved yet, so call scanner to get the next one
	if (!token_available)
	{
		scanner(saved_token, saved_lexeme);
		//cout << "Found token " << token_name[saved_token] << " with word " << saved_lexeme << endl;
		token_available = true;
	}

	end(saved_token);
	return saved_token;
}

// ----- RDP functions - one per non-term -------------------

// ** Make each non-terminal into a function here
// ** Be sure to put the corresponding grammar rule above each function
// i.e. Grammar: 
// ** Be sure to put the name of the programmer above each function
// i.e. Done by:


// Story rule: <story>:= <s> { <s> }
// Done by: Billy
void story()
{
	cout << "Processing <story> " << endl << endl;
	s();

	while (saved_token != EOFM)
	{
		switch (next_token())
		{
		case CONNECTOR:
			s();
			break;

		default:
			s();
			break;
		}
	}

	cout << "Finished processing <story>" << endl;
}


// Checks if the story should be ended
// Done by: Peter
void end(tokentype tokenType)
{
	if (tokenType == EOFM)
	{
		cout << "Successfully parsed <story>. " << endl;
		exit(-1);
	}
}

// S Rule: <s>:= [CONNECTOR] <noun> SUBJECT <after_subject>
// Done by: Billy
void s()
{
	cout << "Processing <s>" << endl;

	next_token();

	if (saved_token == CONNECTOR)
	{
		match(CONNECTOR);
	}

	noun();

	if (next_token() == SUBJECT)
	{
		match(SUBJECT);
	}
	else
	{
		// Expected a SUBJECT, but found some other word
		syntax_error1("SUBJECT", saved_lexeme);
	}

	after_subject();
}

// After Subject rule: 
// <after subject> ::= <verb> <tense> PERIOD | <noun> <after noun>
// Done by: Jordan
void after_subject()
{
	cout << "Processing <after_subject>" << endl;

	switch (next_token())
	{
	case WORD1:
		noun();
		after_noun();
		break;

	case WORD2:
		verb();
		tense();

		if (next_token())
		{
			match(PERIOD);
		}
		else
		{
			syntax_error1("PERIOD", saved_lexeme);
		}
		break;

	default:
		syntax_error2(saved_lexeme, "after_subject()");
	}
}

// After noun rule: 
// <after noun> :: = <be> PERIOD | DESTINATION | <verb> <tense> PERIOD | OBJECT <after object>
// Done by Jordan
void after_noun()
{
	cout << "Processing <after_noun>" << endl;

	switch (next_token())
	{
	case IS:
		be();

		if (next_token() == PERIOD)
		{
			match(PERIOD);
		}
		else
		{
			syntax_error1("PERIOD", saved_lexeme);
		}
		break;

	case WAS:
		be();

		if (next_token() == PERIOD)
		{
			match(PERIOD);
		}
		else
		{
			syntax_error1("PERIOD", saved_lexeme);
		}
		break;

	case DESTINATION:
		match(DESTINATION);
		next_token();
		verb();
		tense();

		if (next_token() == PERIOD)
		{
			match(PERIOD);
		}
		else
		{
			syntax_error1("PERIOD", saved_lexeme);
		}
		break;

	case OBJECT:
		match(OBJECT);
		after_object();
		break;

	default:
		syntax_error2(saved_lexeme, "after_noun()");
		break;
	}
}

// After object rule:
// <after object> ::= <verb> <tense> PERIOD | <noun> DESTINATION <verb <tense> PERIOD
// Done by Peter
void after_object()
{
	cout << "Processing <after_object>" << endl;

	switch (next_token())
	{
	case WORD2:
		verb();
		tense();

		if (next_token() == PERIOD)
		{
			match(PERIOD);
		}
		else
		{
			syntax_error1(token_name[PERIOD], saved_lexeme);
		}

		break;

	case WORD1:
		noun();

		if (next_token() == DESTINATION)
		{
			match(DESTINATION);
		}
		else
		{
			syntax_error1("DESTINATION", saved_lexeme);
		}

		verb();
		tense();

		if (next_token() == PERIOD)
		{
			match(PERIOD);
		}
		else
		{
			syntax_error1("PERIOD", saved_lexeme);
		}

		break;

	default:
		syntax_error2(saved_lexeme, "after_object()");
		break;
	}
}

// Noun rule: <noun> := WORD1 | PRONOUN
// Done by: Jordan
void noun()
{
	cout << "Processing <noun> " << endl;

	switch (next_token())
	{
	   case PRONOUN:
		   match(PRONOUN);
		   break;
	   case WORD1:
		   match(WORD1);
		   break;
	   default:
		   syntax_error2(saved_lexeme, "noun()");
		   break;
	}
}


// Verb rule: <verb> := WORD2
// Done by: Jordan
void verb()
{
	cout << "Processing <verb>" << endl;

	switch (next_token())
	{
	   case WORD2:
		   match(WORD2);
		   break;
	   default:
		   syntax_error2(saved_lexeme, "verb()");
		   break;
	}
}

// be rule: <be> := IS | WAS
// Done by Peter
void be()
{
	cout << "Processing <be>" << endl;

	switch (saved_token)
	{
	case IS:
		match(IS);
		break;

	case WAS:
		match(WAS);
		break;

	default:
		syntax_error2(saved_lexeme, "be()");
	}
}

// tense rule: 
// <tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG
// Done by Jordan
void tense()
{
	cout << "Processing <tense>" << endl;

	switch (next_token())
	{
	case VERBPAST:
		match(VERBPAST);
		break;

	case VERBPASTNEG:
		match(VERBPASTNEG);
		break;

	case VERB:
		match(VERB);
		break;

	case VERBNEG:
		match(VERBNEG);
		break;

	default:
		syntax_error2(saved_lexeme, "tense()");
		break;
	}
}

//---------------------------------------
// The new test driver to start the parser
// Done by:  Billy
int main()
{
	createReservedWordsTable();
	string filename;
	cout << "Enter the input file name: ";
	cin >> filename;

	// fin declared in scanner.h
	fin.open(filename.c_str());

	//** calls the <story> to start parsing
	story();
	//** closes the input file
	fin.close();

}// end
//** require no other input files!
//** syntax error EC requires producing errors.text of messages

