
SubmitIt this file with the executable.

Group#:

1.State who did what by indicating: 
  Member A tasks (syntax error functions) - who: Peter
  Member B tasks (match and next_token)- who: Billy
  Member C tasks (main()) who: Billy

  Who wrote which parser functions? 

  story(), s() --> Billy
  end(), after_object(), be() --> Peter
  verb(), noun(), after_noun(), after_subject(), tense() --> Jordan

If the member did not fully contribute to the assigned part, do not list his/her name!!!!!

2.  Each person came to the meeting?

    Yes

3.  You have included all the required comments? 
   (Grammer rules and Programmer names)


    Yes 

4.  The parser produced correct output for 
    all 6 test cases?

     Yes 

5.  What extra credit feature did you include?
    If it is error correction, did you make sure errors.txt
    got produced?

    Did not implement extra credit features. 

     

